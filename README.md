# RarogCmex's Powersound Gentoo Overlay
Focused primarly to pro audio and video packages, but feel free to sending contributes and pull requests.
My native language is Russian, but I understand English…Well, a bit understand.

### How to add the overlay manually
As a superuser, add to /etc/portage/repos.conf/powersound-overlay.conf the following:

    [powersound-overlay]
    location = /var/db/repos/powersound-overlay
    sync-type = git
    sync-uri = https://gitlab.com/RarogCmex/powersound-overlay.git